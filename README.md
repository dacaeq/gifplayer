# This is my README
#
# Ideas:
* Option to view password-protected file. It must be impossible to tell if a password-protected file even exists. To achieve this, a password is always prompted when the "View pass-protected file" option is chosen. If the password entered doesn't link to an already created password-protected file, the option to register a new password-protected file with this new password will be given. If the password does link to a password-protected file, it will open that file. 