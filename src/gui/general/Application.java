package gui.general;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;

/**
 * Main application class.
 * @author Clayton Ketner
 *
 */
@SuppressWarnings("serial")
public class Application extends JFrame
{	
	Dimension size = new Dimension(600, 400);

	public Application()
	{
		super("GIF Viewer");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //TODO Change to save script

		MainPanel mainPanel = new MainPanel(size);
		
		this.getContentPane().add(mainPanel, BorderLayout.CENTER);
		
		this.pack();
		this.setVisible(true);
		this.setSize(size);
	}
	
	public static void main(String[] args)
	{
		new Application();
	}
}
