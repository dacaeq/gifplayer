package gui.general;

import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * The viewing area panel.
 * @author Clayton Ketner
 *
 */
@SuppressWarnings("serial")
public class ViewingPanel extends JPanel
{
	
	public ViewingPanel()
	{
		this.add(new JButton("Viewing"));
	}
}
