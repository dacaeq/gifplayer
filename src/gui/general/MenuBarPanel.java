package gui.general;


import gui.sidebar.SidebarContentPanel;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class MenuBarPanel extends JPanel implements ActionListener
{
	private SidebarContentPanel sidebarContentPanel;
	
	private static HashMap<ImageEnum, ImageIcon> images;
	private static Boolean isInitialized;
	
	private JButton addButton;
	
	private final int thisHeight = 32;
	private final int padding = 4;

	public MenuBarPanel()
	{
		tryInitialize();
		
		this.setLayout(new BorderLayout());
		this.setPreferredSize(new Dimension(0,thisHeight));
		
		addButton = new JButton("+");
		addButton.addActionListener(this);
		addButton.setActionCommand("addButton");
		this.add(addButton, BorderLayout.WEST);
		addButton.setBorder(null);
		addButton.setPreferredSize(new Dimension(thisHeight - 2*padding, thisHeight - 2*padding));
		
		this.setBorder(BorderFactory.createEmptyBorder(padding, padding, padding, padding));
	}

	private static void tryInitialize()
	{
		if (isInitialized != null)
			return;
		isInitialized = new Boolean(true);
		
		//TODO
	}
	
	public void setSidebarContentPanel(SidebarContentPanel sbcp)
	{
		this.sidebarContentPanel = sbcp;
	}
	
	@Override
	public void actionPerformed(ActionEvent ae)
	{
		if (ae.getActionCommand().equals(addButton.getActionCommand()))
		{
			sidebarContentPanel.newItem();
		}
	}
}
