package gui.general;

import gui.sidebar.SidebarPanel;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JSplitPane;

/**
 * Contains the split pane.
 * @author Clayton Ketner
 *
 */
@SuppressWarnings("serial")
public class MainPanel extends JPanel
{
	MenuBarPanel menuBar;
	JSplitPane splitPane;
	SidebarPanel sidebarPanel;
	ViewingPanel viewingPanel;

	public MainPanel(Dimension size)
	{
		this.setPreferredSize(size);
		this.setSize(size);
		this.setLayout(new BorderLayout());
		
		menuBar = new MenuBarPanel();
		this.add(menuBar, BorderLayout.NORTH);
		
		sidebarPanel = new SidebarPanel();
		viewingPanel = new ViewingPanel();
		
		splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, sidebarPanel, viewingPanel);
		splitPane.setDividerLocation(175);
		
		this.add(splitPane, BorderLayout.CENTER);
		
		menuBar.setSidebarContentPanel(sidebarPanel.getSidebarContentPanel());
	}
}
