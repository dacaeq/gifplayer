package gui.sidebar;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 * Contains the scroll pane for the side bar.
 * @author Clayton Ketner
 *
 */
@SuppressWarnings("serial")
public class SidebarPanel extends JPanel
{
	JScrollPane scrollPane;
	SidebarContentPanel scPanel;

	public SidebarPanel()
	{
		this.setLayout(new BorderLayout());
		
		scPanel = new SidebarContentPanel();
		scrollPane = new JScrollPane(scPanel);
		scrollPane.setBorder(null);
		scrollPane.setMinimumSize(scPanel.getMinimumSize());
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.getVerticalScrollBar().setUnitIncrement(10);
		
		this.add(scrollPane, BorderLayout.CENTER);
		
		scPanel.setScrollPane(scrollPane);
		this.setBorder(null);
	}
	
	public SidebarContentPanel getSidebarContentPanel()
	{
		return scPanel;
	}
}