package gui.sidebar;

import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 * Contains all folders and sidebar items.
 * @author Clayton Ketner
 *
 */
@SuppressWarnings("serial")
public class SidebarContentPanel extends JPanel
{
	private ArrayList<SidebarItem> items = new ArrayList<SidebarItem>();
	private JScrollPane scrollPane;
	
	public SidebarContentPanel()
	{
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
	}
	
	public void setScrollPane(JScrollPane scrollPane)
	{
		this.scrollPane = scrollPane;
	}
	
	public void newItem()
	{
		SidebarItem si = new SidebarItem("Name", "Url");
		this.add(si);
		items.add(si);
		
		// Refresh formatting to ensure proper layout and sizing/use of scroll bars
		scrollPane.validate();
	}
}
