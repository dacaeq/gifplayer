package gui.sidebar;

import gui.general.ImageEnum;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.border.EtchedBorder;

/**
 * A slot for a single gif.
 * @author Clayton Ketner
 *
 */
@SuppressWarnings("serial")
public class SidebarItem extends JPanel implements ActionListener, MouseListener, ComponentListener
{
	private String name, url;
	private JLabel nameLabel;
	private JTextField nameField;
	private final long creationTime;
	private Timer highlightTimer;
	
	private static HashMap<ImageEnum, ImageIcon> images;
	private static Boolean isInitialized = null;
	
	private JButton favoriteButton;
	private boolean isFavorite = false;
	
	private int favoriteSize = 30; // Side length of favorite button
	private Dimension minSize = new Dimension(120, favoriteSize);
	
	
	public SidebarItem(String name, String url)
	{
		this.name = name;
		this.url = url;
		this.creationTime = System.currentTimeMillis();
		highlightTimer = new Timer(50,this);
		highlightTimer.setActionCommand("highlightTimer");
		highlightTimer.addActionListener(this);
		highlightTimer.start();
		
		this.tryInitialize();
		this.addMouseListener(this); // Allows anywhere to be double clicked to edit the name label
		this.addComponentListener(this);
		
		this.setSize(minSize);
		this.setMinimumSize(minSize);
		this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		this.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
		
		nameLabel = new JLabel(name);
		nameLabel.setVisible(true);
		this.add(nameLabel);
				
		// Text field that is shown when editing the field
		nameField = new JTextField();
		nameField.addActionListener(this);
		nameField.setVisible(false);
		this.add(nameField);
		
		nameField.getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "cancel input");
		nameField.getActionMap().put("cancel input", new CancelChange());
		
		String confirmAMKey = "confirm input";
		nameField.getInputMap().put(KeyStroke.getKeyStroke("RETURN"), confirmAMKey);
		nameField.getInputMap().put(KeyStroke.getKeyStroke("ENTER"), confirmAMKey);
		nameField.getActionMap().put(confirmAMKey, new ConfirmChange());
		nameField.setMaximumSize(minSize);
		
		this.add(Box.createHorizontalGlue());
		
		favoriteButton = new JButton(images.get(ImageEnum.UNSTARRED));
		favoriteButton.setPressedIcon(images.get(ImageEnum.PRESSED));
		favoriteButton.setActionCommand("favoriteButton");
		favoriteButton.setMaximumSize(new Dimension(favoriteSize, favoriteSize));
		favoriteButton.setBorder(null);
		favoriteButton.addActionListener(this);
		this.add(favoriteButton);
	}
	
	private void tryInitialize()
	{
		if (isInitialized != null)
			return;
		isInitialized = new Boolean(true);
		
		images = new HashMap<ImageEnum, ImageIcon>();
		
		ImageIcon pressed = new ImageIcon("images/favorite_button/pressed.png");
		images.put(ImageEnum.PRESSED, pressed);
		ImageIcon starred = new ImageIcon("images/favorite_button/starred.png");
		images.put(ImageEnum.STARRED, starred);
		ImageIcon unstarred = new ImageIcon("images/favorite_button/unstarred.png");
		images.put(ImageEnum.UNSTARRED, unstarred);
		
		for (int i=0; i<images.size(); i++)
		{
			ImageEnum imgKey = (ImageEnum) images.keySet().toArray()[i];
			ImageIcon img = images.get(imgKey);
			
			BufferedImage buffImg = new BufferedImage(img.getIconWidth(), img.getIconHeight(), 
													  BufferedImage.TYPE_INT_ARGB);
			Graphics2D g2d = (Graphics2D)buffImg.getGraphics();
			img.paintIcon(null, g2d, 0, 0); // Draw img to the buffImg
			g2d.dispose();
			
			BufferedImage scaledImg = new BufferedImage(favoriteSize, favoriteSize, buffImg.getType());
			Graphics2D gfx = scaledImg.createGraphics();
			gfx.drawImage(buffImg, 0, 0, favoriteSize, favoriteSize, null); // Draw scaled buffImg to scaledImg
			gfx.dispose();
			
			// Replace the unscaled image with the scaled one
			images.put(imgKey, new ImageIcon(scaledImg));
		}
	}
	
	public boolean setLabel(String newName)
	{
		if (newName.length() > 0)
		{
			nameLabel.setText(newName);
			return true;
		}
		return false;
	}
	
	public String getLabelText()
	{
		return name;
	}
	
	public String getURL()
	{
		return url;
	}

	@Override
	public void actionPerformed(ActionEvent ae)
	{
		if (ae.getActionCommand().equals(favoriteButton.getActionCommand()))
		{
			// Swap button image
			if (isFavorite)
			{
				isFavorite = false;
				favoriteButton.setIcon(images.get(ImageEnum.UNSTARRED));
				favoriteButton.setToolTipText("Favorite");
			}
			else
			{
				isFavorite = true;
				favoriteButton.setIcon(images.get(ImageEnum.STARRED));
				favoriteButton.setToolTipText("Unfavorite");
			}
		}
		
		if (ae.getActionCommand().equals(highlightTimer.getActionCommand()))
		{
			// Fade should last 10 seconds + 1 second fade to default background color
			double fadeToWhiteTime = 6;
			double fadeToDefaultTime = 1;
			
			double elapsedTime = (double)(System.currentTimeMillis() - creationTime)/1000.0;
			
			if (elapsedTime < fadeToWhiteTime)
			{
				// Fade to white
				this.setBackground(new Color(255, 255, (int)(elapsedTime/fadeToWhiteTime*255)));
			}
			else if (elapsedTime < (fadeToWhiteTime + fadeToDefaultTime))
			{
				// Fade to default background (238,238,238)
				int rgbValue = (int)((fadeToDefaultTime - (elapsedTime - fadeToWhiteTime))
									 /fadeToDefaultTime*(255-238) + 238);
				this.setBackground(new Color(rgbValue, rgbValue, rgbValue));
			}
			else
			{
				// Set background to default and stop the timer
				this.setBackground(UIManager.getColor("Panel.background"));
				highlightTimer.stop();
			}
		}
	}

	@Override
	public void mouseClicked(MouseEvent e)
	{
		if (e.getSource().equals(this))
		{
			if (e.getClickCount() == 2)
			{
				// Double clicked - allow editing
				nameLabel.setVisible(false);
				nameLabel.setEnabled(false);
				
				nameField.setPreferredSize(new Dimension(this.getWidth() - favoriteSize - 
						2*this.getBorder().getBorderInsets(this).left, this.getHeight()));
				nameField.setVisible(true);
				nameField.setEnabled(true);
				nameField.setText(name);
				nameField.requestFocus();
				nameField.selectAll();
			}
		}
	}
	
	@Override
	public void mouseEntered(MouseEvent e)
	{
		// Not used
	}

	@Override
	public void mouseExited(MouseEvent e)
	{
		// Not used
	}

	@Override
	public void mousePressed(MouseEvent e)
	{
		// Not used
	}

	@Override
	public void mouseReleased(MouseEvent e)
	{
		// Not used
	}
	
	public int getTextArea()
	{
		return this.getWidth() - favoriteSize - 10;
	}
	
	class ConfirmChange extends AbstractAction
	{	
		@Override
		public void actionPerformed(ActionEvent e)
		{			
			if (nameField.isEnabled())
			{
				if (nameField.getText().length() > 0)
				{
					name = nameField.getText();
					truncateNameLabel();
				}
								
				nameField.setEnabled(false);
				nameField.setVisible(false);
				
				nameLabel.setEnabled(true);
				nameLabel.setVisible(true);
			}
		}
	}
	
	class CancelChange extends AbstractAction
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			if (nameField.isEnabled())
			{
				nameField.setEnabled(false);
				nameField.setVisible(false);
				
				nameLabel.setEnabled(true);
				nameLabel.setVisible(true);
			}
		}
	}

	@Override
	public void componentHidden(ComponentEvent e)
	{
		// Not used
	}

	@Override
	public void componentMoved(ComponentEvent e)
	{
		// Not used		
	}

	@Override
	public void componentResized(ComponentEvent e)
	{
		if (nameLabel != null && nameLabel.isVisible())
		{
			truncateNameLabel();
		}
	}
	
	protected void truncateNameLabel()
	{
		int originalWidth = this.getTextArea();
		nameLabel.setText(name);
		
		while (nameLabel.getPreferredSize().width > originalWidth)
		{
			String labelText = nameLabel.getText();
			
			// If the text already has an ellipsis, remove it
			if (labelText.substring(labelText.length()-3, labelText.length()).equals("..."))
			{
				nameLabel.setText(labelText.substring(0, labelText.length()-3));
			}
			
			nameLabel.setText(nameLabel.getText().substring(0, nameLabel.getText().length()-1));
			nameLabel.setText(nameLabel.getText().concat("..."));
		}
	}

	@Override
	public void componentShown(ComponentEvent e)
	{
		// Not used
	}
}






































